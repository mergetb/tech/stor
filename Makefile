SHELL:=/bin/bash

all: build

build: *.go
	go build $^
