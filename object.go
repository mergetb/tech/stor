package stor

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	//etcd "github.com/coreos/etcd/clientv3"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
)

// An Object implements the struct for all data in the etcd datastore.
type Object interface {
	Key() string
	GetVersion() int64
	SetVersion(int64)
	Value() interface{}
}

// ErrNotFound is the default error returned when an object is not found in datastore.
var ErrNotFound error = fmt.Errorf("not found")

// Read checks if the object exists in datastore.
// The object in the datastore is written to the object passed in.
// An error is return if access to the datastore is not possible, or if no object is found.
func Read(obj Object) error {

	n, err := ReadObjects([]Object{obj})
	if err != nil {
		return err
	}
	if n == 0 {
		return ErrNotFound
	}

	return nil
}

// ReadNew calls Read, but does not return an error if the object is not found.
func ReadNew(obj Object) error {

	err := Read(obj)
	if err != nil && err != ErrNotFound {
		return err
	}

	return nil
}

// ReadTimer should be in config
type ReadTimer struct {
	Period  time.Duration
	Timeout time.Duration
}

// ReadWait attempts to read an object repeatedly until a timeout threshold is
// reached defined by timer. If timer is nil the defaults of 30 seconds with a
// retry period of 250 milliseconds is applied
func ReadWait(obj Object, timer *ReadTimer) error {

	if timer == nil {
		timer = &ReadTimer{
			Period:  250 * time.Millisecond,
			Timeout: 30 * time.Second,
		}
	}

	count := 0
	var err error
	for err = ErrNotFound; err == ErrNotFound; err = Read(obj) {
		time.Sleep(timer.Period)
		count++
		if time.Duration(count)*timer.Period > timer.Timeout {
			break
		}
	}

	return err
}

// ReadObjects is the core function to all Read* functions.
// It takes an array of objects, forms a transaction, and updates
// each object based on the existance of the object's key in the datastore.
func ReadObjects(objs []Object) (int, error) {

	var ops []clientv3.Op
	omap := make(map[string]Object)

	names := make([]string, 0)
	for _, o := range objs {
		names = append(names, o.Key())
		omap[o.Key()] = o
		ops = append(ops, clientv3.OpGet(o.Key()))
	}

	eConfig := GetConfig()

	n := 0
	objsize := 0
	err := WithEtcd(func(c *clientv3.Client) error {

		kvc := clientv3.NewKV(c)
		ctx, cancel := context.WithTimeout(context.TODO(), eConfig.Timeout)

		resp, err := kvc.Txn(ctx).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("")
		}

		for _, r := range resp.Responses {
			rr := r.GetResponseRange()
			if rr == nil {
				continue
			}

			for _, kv := range rr.Kvs {
				n++
				o := omap[string(kv.Key)]

				switch t := o.Value().(type) {
				case *string:
					*t = string(kv.Value)
				default:
					FromJSON(o, kv.Value)
					o.SetVersion(kv.Version)
				}
				objsize += len([]byte(kv.Value))
			}
		}

		return nil

	})
	if err != nil {
		return 0, fmt.Errorf("failed to read", err)
	}
	log.Tracef("Read (%v) Size: %d", names, objsize)

	return n, err

}

// Write is a wrapper for WriteObjects using single object
func Write(obj Object, fresh bool, opts ...clientv3.OpOption) error {
	return WriteObjects([]Object{obj}, fresh, opts...)
}

// WriteObjects writes objects to the datastore in a single shot transaction. If
// fresh is true, then all objects must be the most recent version, or the write
// will fail.
func WriteObjects(objs []Object, fresh bool, opts ...clientv3.OpOption) error {

	var ops []clientv3.Op
	var ifs []clientv3.Cmp

	eConfig := GetConfig()

	names := make([]string, 0)
	objsize := 0
	for _, obj := range objs {
		names = append(names, obj.Key())

		var value string
		switch t := obj.Value().(type) {
		case *string:
			value = *t
		default:
			value = ToJSON(obj)
		}
		objsize += len([]byte(value))

		ops = append(ops, clientv3.OpPut(obj.Key(), value, opts...))
		if fresh {
			ifs = append(ifs,
				clientv3.Compare(clientv3.Version(obj.Key()), "=", obj.GetVersion()))
		}
	}

	log.Tracef("Write (%v) Size: %d", names, objsize)

	return WithEtcd(func(c *clientv3.Client) error {
		kvc := clientv3.NewKV(c)
		if kvc == nil {
			log.Error("failed to create etcd client")
			return fmt.Errorf("failed to create etcd client")
		}

		ctx, cancel := context.WithTimeout(context.TODO(), eConfig.Timeout)
		resp, err := kvc.Txn(ctx).If(ifs...).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("state has changed since read")
		}

		for _, o := range objs {
			o.SetVersion(o.GetVersion() + 1)
		}
		return nil
	})
}

// Touch update the key, but not value in data store
func Touch(obj Object) error {
	return TouchObjects([]Object{obj})
}

// TouchObjects updates multiple keys
func TouchObjects(objs []Object) error {

	var ops []clientv3.Op
	var ifs []clientv3.Cmp

	names := make([]string, 0)
	objsize := 0
	for _, obj := range objs {
		names = append(names, obj.Key())

		ops = append(ops, clientv3.OpPut(obj.Key(), "", clientv3.WithIgnoreValue()))
	}

	log.Tracef("Write (%v) Size: %d", names, objsize)

	return WithEtcd(func(c *clientv3.Client) error {

		kvc := clientv3.NewKV(c)
		if kvc == nil {
			log.Error("failed to create etcd client")
			return fmt.Errorf("failed to create etcd client")
		}

		ctx, cancel := context.WithTimeout(context.TODO(), 1*time.Minute)
		resp, err := kvc.Txn(ctx).If(ifs...).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("state has changed since read")
		}
		return nil
	})
}

// DeleteObjects remove objects from datastore
func DeleteObjects(objs []Object) error {

	var ops []clientv3.Op

	for _, obj := range objs {
		ops = append(ops, clientv3.OpDelete(obj.Key()))
	}

	return WithEtcd(func(c *clientv3.Client) error {

		eConfig := GetConfig()

		kvc := clientv3.NewKV(c)
		ctx, cancel := context.WithTimeout(context.TODO(), eConfig.Timeout)
		resp, err := kvc.Txn(ctx).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("delete objects failed")
		}
		return nil
	})
}

// Delete is a wrapper for DeleteObjects. Delete a single object
func Delete(obj Object) error {
	return DeleteObjects([]Object{obj})
}

// An ObjectTx supports put and delete operations
type ObjectTx struct {
	Put    []Object
	Delete []Object
}

// RunObjectTx runs an object transaction.
func RunObjectTx(otx ObjectTx) error {

	var ops []clientv3.Op
	eConfig := GetConfig()

	names := make([]string, 0)
	objsize := 0
	for _, obj := range otx.Put {
		names = append(names, obj.Key())

		var value string
		switch t := obj.Value().(type) {
		case *string:
			value = *t
		default:
			value = ToJSON(obj)
		}
		objsize += len([]byte(value))

		ops = append(ops, clientv3.OpPut(obj.Key(), string(value)))
	}

	log.Tracef("WriteTx (%v) Size: %d", names, objsize)
	names = make([]string, 0)
	for _, x := range otx.Delete {
		names = append(names, x.Key())
		ops = append(ops, clientv3.OpDelete(x.Key()))
	}
	log.Tracef("DeleteTx: (%v)", names)

	return WithEtcd(func(c *clientv3.Client) error {
		kvc := clientv3.NewKV(c)
		ctx, cancel := context.WithTimeout(context.TODO(), eConfig.Timeout)
		resp, err := kvc.Txn(ctx).Then(ops...).Commit()
		cancel()

		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("run object txn failed")
		}
		return nil
	})
}

// TxnFailedPrefix prefix for failed transactions
const TxnFailedPrefix = "txn failed"

// TxnFailed function first logs the TxnFailedPrefix+message before returning an error
func TxnFailed(message string) error {
	err := fmt.Errorf("%s: %s", TxnFailedPrefix, message)
	log.Error(err)
	return err
}

// IsTxnFailed checks the error message for the TxnFailedPrefix
func IsTxnFailed(err error) bool {
	return strings.HasPrefix(err.Error(), TxnFailedPrefix)
}

// FromJSON reads reads on object from byte array encoded json. If the object is
// a protobuf, then protobuf is used instead.
func FromJSON(o Object, b []byte) {

	// if this is a protobuf, unmarshal as such
	msg, ok := o.Value().(proto.Message)
	if ok {
		log.Trace("from: as protobuf")
		err := jsonpb.Unmarshal(bytes.NewReader(b), msg)
		if err == nil {
			return
		}
		log.Warnf("fail to unmarshal once, %v, %#v", err, log.Fields{"object": b})
		//fallthrough to json
	}

	err := json.Unmarshal(b, o.Value())
	if err != nil {
		panic(err) //all merge objects must be json serializable, the end
	}

}

// ToJSON marshals an object to JSON form. If the object is a protobuf, protobuf
// is used instead.
func ToJSON(o Object) string {

	// if this is a protobuf, marshal as such
	msg, ok := o.Value().(proto.Message)
	if ok {
		log.Trace("from: as protobuf")
		var buf bytes.Buffer
		m := jsonpb.Marshaler{}
		err := m.Marshal(&buf, msg)
		if err == nil {
			return buf.String()
		}
		log.Warnf("fail to pbmarshal , %v, %#v", err, log.Fields{"object": o})
		//fallthrough to json
	}

	buf, err := json.MarshalIndent(o.Value(), "", "  ")
	if err != nil {
		panic(err) //all merge objects must be json serializable, the end
	}
	return string(buf)
}
