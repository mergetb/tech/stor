package stor

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"time"

	//"github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
)

var c *Config

// A Config is the struct containing the etcd datastor configuration options.
// There is a single config, one instance per library, not modifiable outside library
// accessed through GetConfig()
type Config struct {
	Address string
	Port    int
	TLS     *TLSConfig
	Quantum time.Duration
	Timeout time.Duration
}

// A TLSConfig is the struct containing TLS options for connecting to the etcd datastore
type TLSConfig struct {
	Cacert string
	Cert   string
	Key    string
}

// GetConfig returns the etcd configuration.
func GetConfig() Config {
	return *c
}

// WithEtcd wraps etcd connection, handling closing connection when done.
func WithEtcd(f func(*clientv3.Client) error) error {

	cli, err := etcdConnect()
	if err != nil {
		return err
	}
	defer cli.Close()

	return f(cli)

}

// etcdClient gets the global etcd configuration and returns an etcd v3 client
func etcdClient() (*clientv3.Client, error) {

	log.Trace("creating new client...")
	c := GetConfig()

	var tlsc *tls.Config
	if c.TLS != nil {

		log.Trace("etcd tls enabled")

		log.WithFields(log.Fields{
			"cacert": c.TLS.Cacert,
			"cert":   c.TLS.Cert,
			"key":    c.TLS.Key,
		}).Trace("tls config")

		capool := x509.NewCertPool()
		capem, err := ioutil.ReadFile(c.TLS.Cacert)
		if err != nil {
			return nil, fmt.Errorf("failed to read cacert %v", err)
		}
		ok := capool.AppendCertsFromPEM(capem)
		if !ok {
			return nil, fmt.Errorf("capem is not ok %v", log.Fields{"ok": ok})
		}

		cert, err := tls.LoadX509KeyPair(
			c.TLS.Cert,
			c.TLS.Key,
		)
		if err != nil {
			return nil, fmt.Errorf("failed to load cert/key pair %v", err)
		}

		tlsc = &tls.Config{
			RootCAs:      capool,
			Certificates: []tls.Certificate{cert},
		}
	} else {
		log.Trace("etcd tls disabled")
	}

	connstr := fmt.Sprintf("%s:%d", c.Address, c.Port)
	log.WithFields(log.Fields{
		"connstr": connstr,
	}).Trace("etcd connection string")

	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{connstr},
		DialTimeout: c.Timeout,
		TLS:         tlsc,
	})

	log.Trace("client created")
	return cli, err

}

// SetConfig configure the global etcd settings
func SetConfig(conf Config) {
	c = new(Config)
	*c = conf
}

// etcdConnect returns an etcd v3 client, with a max of 10 retries
func etcdConnect() (*clientv3.Client, error) {
	log.Trace("connecting to etcd...")

	var c Config

	etcd, err := etcdClient()
	if err == nil {
		// if we do have an error, get the configuration
		c = GetConfig()
		return etcd, nil
	}
	for i := 0; i < 10; i++ {
		log.Errorf("error connecting to etcd %v", err)
		time.Sleep(c.Quantum)
		etcd, err = etcdClient()
		if err == nil {
			return etcd, nil
		}
	}

	return nil, fmt.Errorf("failed to connect to etcd %v", err)
}
