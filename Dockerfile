FROM golang:bullseye as build

RUN apt update && apt upgrade -qy
RUN apt install -y \
    build-essential \
    golang \
    make \
    ca-certificates \
    protobuf-compiler \
    vim

RUN mkdir -p /go
ENV GOPATH /go
ENV GOBIN /go/bin
ENV PATH "$PATH:$GOBIN"
RUN go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
RUN go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2


COPY . /stor
WORKDIR /stor

RUN go mod tidy
RUN rm -rf build
RUN make
